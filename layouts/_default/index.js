var docs = [
{{ range $index, $page := (where .Site.Pages "Section" .Site.Params.docsSection) -}}
  {
    id: {{ $index }},
    title: "{{ .Title }}",
    description: "{{ .Description }}",
    href: "{{ .URL | relURL }}"
  },
{{ end -}}
];