const localMode = localStorage.getItem('theme');

// Default to dark mode
if (localMode === null) {

  localStorage.setItem('theme', 'dark');
  document.documentElement.setAttribute('data-dark-mode', '');

}

if (localMode === 'dark') {

  document.documentElement.setAttribute('data-dark-mode', '');

}

if (localMode === 'light') {

  document.documentElement.removeAttribute('data-dark-mode');

}