---
title: "Resources I Recommend"
description: "An assortment of informational resources and publications for the intellectually curious."
lead: "An assortment of informational resources and publications for the intellectually curious."
draft: false
tags: []

date: 2022-04-13
menu:
  guides:
    parent: "misc"
weight: 999
toc: true
excludeFromFeeds: true
outputs:
- HTML
- RSS

uuid: "8d6ded71-d2b0-46c5-8e70-072ec60bb94b-2"
---

{{< alert icon="👉" >}}
This page is expected to be updated over time and will never be considered 'final.'

For that reason, this page has [its own Atom feed]({{< alt-format format="RSS" >}}) and is excluded from the sitewide feeds.
{{< /alert >}}

Other than alphabetization, this list is in no particular order.

_Descriptions coming soon&trade;._ <!-- TODO -->

## Audio Hardware

- [AutoEq](https://github.com/jaakkopasanen/AutoEq)
- [crinacle](https://crinacle.com/)
- [oratory1990](https://www.reddit.com/r/oratory1990/wiki/)
- [Techmoan](https://www.youtube.com/c/Techmoan/videos)

## Civil Engineering and Construction

- [Ashville](https://www.youtube.com/c/thisisashville/videos)
- [Building Integrity](https://www.youtube.com/c/BuildingIntegrity/videos)
- [City Beautiful](https://www.youtube.com/c/CityBeautiful/videos) (also on [Nebula](https://nebula.app/citybeautiful))
- [Not Just Bikes](https://www.youtube.com/c/NotJustBikes/videos) (also on [Nebula](https://nebula.app/notjustbikes))
- [Practical Engineering](https://www.youtube.com/c/PracticalEngineeringChannel/videos) (also on [Nebula](https://nebula.app/practical-engineering))
- [Road Guy Rob](https://www.youtube.com/c/RoadGuyRob/videos)

## Computer Hardware and Software

- [apalrd's adventures](https://www.youtube.com/c/apalrdsadventures/videos) and his [blog](https://www.apalrd.net/)
- [Blocks and Files](https://blocksandfiles.com/)
- [Chyrosran22](https://www.youtube.com/user/Chyrosran22/videos)
- [Ctrl blog](https://www.ctrl.blog/)
- [der8auer](https://www.youtube.com/channel/UCGsaijjOJshS2_ZmMNZgS-g/videos)
- [DistroWatch](https://distrowatch.com/)
- [Gamers Nexus](https://www.youtube.com/c/GamersNexus/videos)
- [Hacker News](https://news.ycombinator.com/)
- [Hardware Haven](https://www.youtube.com/c/HardwareHaven/videos)
- [r/homelab](https://old.reddit.com/r/homelab/) and [r/homelab wiki](https://old.reddit.com/r/homelab/wiki/)
- [Jeff Geerling](https://www.youtube.com/c/JeffGeerling/videos)
- [Lawrence Systems](https://www.youtube.com/user/TheTecknowledge/videos) and [Lawrence Systems forum](https://forums.lawrencesystems.com/)
- [Level1Techs](https://www.youtube.com/c/Level1Techs/videos) and [Level1Linux](https://www.youtube.com/c/TekLinux/videos)
- [Linux Uprising](https://www.linuxuprising.com/)
- [Phoronix](https://www.phoronix.com/)
- [Rip It Apart – Jason's electronics blog-thingy](https://ripitapart.com/)
- [Seirdy](https://seirdy.one/)
- [ServeTheHome](https://www.servethehome.com/) and [STH forum](https://forums.servethehome.com/)
- [StorageReview.com](https://www.storagereview.com/)
- [TechTechPotato](https://www.youtube.com/c/TechTechPotato/videos)
- [TuxPhones](https://tuxphones.com/)
- [Tyblog](https://blog.tjll.net/)

## Cybersecurity and Privacy

- [BleepingComputer](https://www.bleepingcomputer.com/)
- [Krebs on Security](https://krebsonsecurity.com/)
- [Lawrence Systems](https://www.youtube.com/user/TheTecknowledge/videos) and [Lawrence Systems forum](https://forums.lawrencesystems.com/)
- [Live Overflow](https://www.youtube.com/c/LiveOverflow/videos)
- Cybersecurity Tips: ["👻 Online privacy: to what extent should you try to go dark?"](https://cyb3rsecurity.tips/p/-online-privacy-to-what-extent-should)
  - <small>_I have very mixed feelings about other content on Cybersecurity Tips._</small>
- [Positive Security blog](https://positive.security/blog)
- [Privacy Guides](https://www.privacyguides.org/)
- [PrivSec.dev](https://privsec.dev/)
- [PwnFunction](https://www.youtube.com/c/PwnFunction/videos)
- [Seirdy](https://seirdy.one/)
- [Wonderfall](https://wonderfall.dev/)

## Electric Vehicles

- [Bjørn Nyland](https://www.youtube.com/user/bjornnyland/videos) and his [test results](https://drive.google.com/open?id=1HOwktdiZmm40atGPwymzrxErMi1ZrKPP)
- [News Coulomb](https://www.youtube.com/channel/UCJqgqWqKmdkIuBZa7JK5KSw/videos)

## Electronics and Electrical Engineering

- [Asianometry](https://www.youtube.com/c/Asianometry/videos) and the [Asianometry newsletter](https://asianometry.substack.com/archive)
- [bigclivedotcom](https://www.youtube.com/c/Bigclive/videos) (also on [Odysee](https://odysee.com/@bigclivedotcom:0d?view=content)/[LBRY](lbry://@bigclivedotcom#0d))
- [CuriousMarc](https://www.youtube.com/c/CuriousMarc/videos)
- [EEVblog](https://www.youtube.com/c/EevblogDave/videos) (also on [Odysee](https://odysee.com/@eevblog:7?view=content)/[LBRY](lbry://@eevblog#7) and [Utreon](https://utreon.com/c/EEVblog/videos)) and [EEVblog forum](https://www.eevblog.com/forum/)
- [ElectroBOOM](https://www.youtube.com/c/Electroboom/videos) (also on [Odysee](https://odysee.com/@ElectroBOOM:9?view=content)/[LBRY](lbry://@ElectroBOOM#9))
- [Julian Ilett](https://www.youtube.com/c/JulianIlett/videos) (also on [Odysee](https://odysee.com/@julian256:d?view=content)/[LBRY](lbry://@julian256#d
))
- [Louis Rossmann](https://www.youtube.com/user/rossmanngroup/videos) (also on [Odysee](https://odysee.com/@rossmanngroup:a?view=content)/[LBRY](lbry://@rossmanngroup#a)) and his [beginner's guide to electronics](https://docs.google.com/presentation/d/1PkeO_lC5WTPScSV3ZzEEjVuDWeQtL2eHK6jEcf7axA0/edit)
- [Marco Reps](https://www.youtube.com/c/MarcoReps/videos) (also on [Odysee](https://odysee.com/@reps:3?view=content)/[LBRY](lbry://@reps#3))
- [Repair Wiki](https://repair.wiki/)
- [Rip It Apart – Jason's electronics blog-thingy](https://ripitapart.com/)
- [Strange Parts](https://www.youtube.com/c/StrangeParts/videos) (also on [Nebula](https://nebula.app/strangeparts))
- [Technology Connections](https://www.youtube.com/c/TechnologyConnections/videos) and [Technology Connextras](https://www.youtube.com/c/TechnologyConnections2/videos)

## Flashlights

- [r/flashlight](https://old.reddit.com/r/flashlight/)
- [Grizzly's Reviews](https://grizzlysreviews.wordpress.com/)
- [ZeroAir Reviews](https://zeroair.org/)

## General Math and Science

- [Bartosz Ciechanowski](https://ciechanow.ski/archives/)
- [Brainiac75](https://www.youtube.com/c/brainiac75/videos)
- [Quanta Magazine](https://www.quantamagazine.org/) and their [YouTube channel](https://www.youtube.com/c/QuantaScienceChannel/videos)
- [standupmaths](https://www.youtube.com/user/standupmaths/videos)
- [Technology Connections](https://www.youtube.com/c/TechnologyConnections/videos) and [Technology Connextras](https://www.youtube.com/c/TechnologyConnections2/videos)
- [Veritasium](https://www.youtube.com/c/veritasium/videos) (also on [Odysee](https://odysee.com/@veritasium:f?view=content)/[LBRY](lbry://@veritasium#f))

## Human Geography and Economics

- [Asianometry](https://www.youtube.com/c/Asianometry/videos) and the [Asianometry newsletter](https://asianometry.substack.com/archive)
- [Company Man](https://www.youtube.com/channel/UCQMyhrt92_8XM0KgZH6VnRg/videos)
- [PolyMatter](https://www.youtube.com/c/PolyMatter/videos) (also on [Nebula](https://nebula.app/polymatter))
- [RealLifeLore](https://www.youtube.com/c/RealLifeLore/videos) (also on [Nebula](https://nebula.app/reallifelore))
- [Wendover Productions](https://www.youtube.com/c/Wendoverproductions/videos) (also on [Nebula](https://nebula.app/wendover))

## Law

- [Building Integrity](https://www.youtube.com/c/BuildingIntegrity/videos)
- [Lawful Masses with Leonard French](https://www.youtube.com/c/Lawfulmassesunite/videos)
- [LegalEagle](https://www.youtube.com/c/LegalEagle/videos) (also on [Nebula](https://nebula.app/legaleagle))
- [Steve Lehto](https://www.youtube.com/c/stevelehto/videos)

## Maker

- [Aging Wheels](https://www.youtube.com/user/agingwheels/videos) and [Under Dunn](https://www.youtube.com/c/UnderDunnOfficial/videos)
- [AvE](https://www.youtube.com/c/arduinoversusevil2025/videos)
- [Julian Ilett](https://www.youtube.com/c/JulianIlett/videos) (also on [Odysee](https://odysee.com/@julian256:d?view=content)/[LBRY](lbry://@julian256#d
))
- [LED Gardener](https://www.youtube.com/c/LEDGardener/videos)
- [Maker's Muse](https://www.youtube.com/c/MakersMuse/videos)
- [Marco Reps](https://www.youtube.com/c/MarcoReps/videos) (also on [Odysee](https://odysee.com/@reps:3?view=content)/[LBRY](lbry://@reps#3))

## Physical Security

- [LockPickingLawyer](https://www.youtube.com/c/lockpickinglawyer/videos) and his [talk](https://www.youtube.com/watch?v=IH0GXWQDk0Q)
- [Deviant Ollam](https://www.youtube.com/user/DeviantOllam/videos) and his [many talks](https://deviating.net/lockpicking/videos.html)

## Programming

- [Fireship](https://www.youtube.com/c/Fireship/videos) (also on [Odysee](https://odysee.com/@fireship:6?view=content)/[LBRY](lbry://@fireship#6)), [Beyond Fireship](https://www.youtube.com/channel/UC2Xd-TjJByJyK2w1zNwY0zQ/videos), and Fireship's [paid courses](https://fireship.io/courses/)
- [Hacker News](https://news.ycombinator.com/)
- [Kevin Powell](https://www.youtube.com/kepowob/videos)
- [Low Level Learning](https://www.youtube.com/c/LowLevelLearning/videos)

## Retro Tech

- [LGR](https://www.youtube.com/c/Lazygamereviews/videos)
- [LowSpecGamer](https://www.youtube.com/c/LowSpecGamer/videos) (also on [Nebula](https://nebula.app/lowspecgamer))
- [Michael MJD](https://www.youtube.com/c/MichaelMJD/videos)
- [Modern Vintage Gamer](https://www.youtube.com/c/ModernVintageGamer/videos)
- [NCommander](https://www.youtube.com/c/NCommander/videos)
- [Techmoan](https://www.youtube.com/c/Techmoan/videos)
- [Technology Connections](https://www.youtube.com/c/TechnologyConnections/videos) and [Technology Connextras](https://www.youtube.com/c/TechnologyConnections2/videos)

## Right to Repair

- [Hugh Jeffreys](https://www.youtube.com/c/HughJeffreys/videos)
- [Louis Rossmann](https://www.youtube.com/user/rossmanngroup/videos) (also on [Odysee](https://odysee.com/@rossmanngroup:a?view=content)/[LBRY](lbry://@rossmanngroup#a))
- [Repair Wiki](https://repair.wiki/)
- [Salem Techsperts](https://www.youtube.com/c/SalemTechsperts/videos)
- [U.S. PIRG](https://uspirg.org/feature/usp/right-repair)

## Tool Reviews

- [AvE](https://www.youtube.com/c/arduinoversusevil2025/videos)
- [Project Farm](https://www.youtube.com/c/ProjectFarm/videos)
- [Torque Test Channel](https://www.youtube.com/c/TorqueTestChannel/videos)

## Trades

- [blancolirio](https://www.youtube.com/user/blancolirio/videos)
- [Electrician U](https://www.youtube.com/c/ElectricianU/videos) (also on [Odysee](https://odysee.com/@electricianu:9?view=content)/[LBRY](lbry://@electricianu#9))
- [Hugh Jeffreys](https://www.youtube.com/c/HughJeffreys/videos)
- [HVACR VIDEOS](https://www.youtube.com/c/HVACRVIDEOS/videos)
- [Louis Rossmann](https://www.youtube.com/user/rossmanngroup/videos) (also on [Odysee](https://odysee.com/@rossmanngroup:a?view=content)/[LBRY](lbry://@rossmanngroup#a))
- [ThePoolGuy](https://www.youtube.com/c/ThePoolGuy/videos)

## Other

- [BioArk](https://www.youtube.com/channel/UCVkgJKL07bSQ1b54Pnz186Q/videos) (also on [Nebula](https://nebula.app/bioark))
- [r/BuyItForLife](https://old.reddit.com/r/BuyItForLife/)
- [CGP Grey](https://www.youtube.com/greymatter/videos)
- [Climate Town](https://www.youtube.com/c/ClimateTown/videos)
- [Defunctland](https://www.youtube.com/c/Defunctland/videos)
- [Geoff Marshall](https://www.youtube.com/user/geofftech2/videos)
- [Half as Interesting](https://www.youtube.com/c/halfasinteresting/videos) (also on [Nebula](https://nebula.app/hai))
- Hansen Wholesale [Ceiling Fan FAQs](https://www.hansenwholesale.com/ceiling-fans/fanfaqs)
- [Jim Browning](https://www.youtube.com/c/JimBrowning/videos)
- [Retail Archaeology](https://www.youtube.com/c/RetailArchaeology/videos)
- [Thunderf00t](https://www.youtube.com/c/Thunderf00t/videos)
- [Tom Scott](https://www.youtube.com/c/TomScottGo/videos)

## Honorable Mentions

For various reasons, I have chosen not to include these in the above sections, but I still think they are worth checking out.

- [Atomic Shrimp](https://www.youtube.com/c/AtomicShrimp/videos)
- [Kevin Norman](https://kn100.me/)
- [Linus Tech Tips](https://www.youtube.com/c/LinusTechTips/videos), [Techquickie](https://www.youtube.com/c/Techquickie/videos), and [TechLinked](https://www.youtube.com/c/techlinked/videos) (also on [Floatplane](https://www.floatplane.com/channel/linustechtips))
- [maker.moekoe](https://www.youtube.com/c/makermoekoe/videos) (also on [GitHub](https://github.com/makermoekoe?tab=repositories))
- [Mike Boyd](https://www.youtube.com/c/MikeBoydVideo/videos) (also on [Nebula](https://nebula.app/mikeboyd))
- [scottbez1](https://www.youtube.com/user/scottbez1/videos) (also on [GitHub](https://github.com/scottbez1?tab=repositories))
- [Stuff Made Here](https://www.youtube.com/c/StuffMadeHere/videos)
- [Tom Scott plus](https://www.youtube.com/c/tomscottplus/videos)
