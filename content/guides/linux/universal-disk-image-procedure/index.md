---
title: "Universal Disk Image Procedure"
description: "Turn virtually any disk image file into a bootable drive."
lead: "How to turn virtually any disk image file into a bootable drive."
draft: false
tags: []
lastmod: 2022-06-18

date: 2022-05-16
menu:
  guides:
    parent: "linux"
weight: 1
toc: true

uuid: "abe5db40-50ed-46f2-83c6-f0c9afe2913a"
---

## Verifying your download

Always verify the integrity of your downloaded disk image file before using it. Look for a SHA256 hash or checksum file provided by the publisher.

<details>
<summary>{{< md >}}**Terminal method** (Linux/Mac){{< /md >}}</summary>

### Terminal method (Linux/Mac) {#verify-terminal .hidden-heading}

{{< alert context="dark" icon="👉" >}}
Mac OS users should substitute `sha256sum` with `shasum -a 256`.
{{< /alert >}}

Given a SHA256 checksum file, simply pass that file to `sha256sum`:

{{< terminal >}}
$ <kbd>sha256sum -c <strong>&lt;check_file&gt;</strong></kbd>
&lt;disk_image.iso&gt;: OK
{{< /terminal >}}

If you only have the hash itself, pipe it and the path to the disk image to `sha256sum`:

{{< terminal >}}
$ <kbd>echo '<strong>&lt;hash&gt;</strong>' '<strong>&lt;disk_image.iso&gt;</strong>' | sha256sum -c</kbd>
&lt;disk_image.iso&gt;: OK
{{< /terminal >}}

Note that it may take some time to calculate the hash for larger files.

{{< alert context="danger" icon="👉" >}}
**If you see `FAILED` instead of `OK`, your disk image is corrupted.** Try redownloading it, from a different mirror if possible.
{{< /alert >}}

</details>

<details>
<summary>{{< md >}}**GUI method** (Linux/Windows){{< /md >}}</summary>

### GUI method (Linux/Windows) {#verify-gui .hidden-heading}

Install and run GtkHash for Linux ([Flatpak](https://www.flathub.org/apps/details/org.gtkhash.gtkhash "GtkHash on Flathub")) or [Windows](https://github.com/tristanheaven/gtkhash/releases/latest "GtkHash releases").

If you have a checksum file, open it with <kbd><kbd><samp>File</samp></kbd> > <kbd><samp>Open Digest File...</samp></kbd></kbd>.

If not, click in the `File:` field and open the disk image. Paste the expected hash in the `Check:` field.

Click <kbd><samp>Hash</samp></kbd> to compute the hashes of the selected file. Look for a checkmark or similar symbol (theme-dependent):

![GtkHash screenshot](GtkHash.webp)
{.text-center}

{{< alert context="danger" icon="👉" >}}
**If you do not see a match indicator, your disk image is corrupted.** Try redownloading it, from a different mirror if possible.
{{< /alert >}}

</details>

## Flashing the image

{{< alert context="warning" icon="👉" >}}
Use a spare flash drive with any valuable data backed up elsewhere.<br>
**This operation will wipe all data on the drive!**
{{< /alert >}}

Faster flash drives, especially USB 3.0+, are recommended. External hard drives and SSDs also work in most cases (and tend to be faster).

_Some BIOSes have compatibility issues booting from certain drives --- if you experience this, double-check your BIOS settings and try other (distinct) flash drive models. As a last resort, USB 2.0 flash drives (or any drive connected through a USB 2.0 hub) tend to be just about universally compatible._

{{< alert context="info" icon="👉" >}}
Flashing Windows ISOs requires special tools: [{{< nowrap >}}WoeUSB-ng{{< /nowrap >}}](https://github.com/WoeUSB/WoeUSB-ng) (Linux) or [Rufus](https://rufus.ie/) (Windows).
{{< /alert >}}

<details>
<summary>{{< md >}}**Terminal method** (Linux){{< /md >}}</summary>

### Terminal method (Linux) {#flash-terminal .hidden-heading}

Connect the drive and check its assigned device name (e.g. `sda`). **Make sure to use the correct device name in future steps!**

{{< terminal >}}
$ <kbd>lsblk -S</kbd>
NAME HCTL       TYPE VENDOR   MODEL          REV SERIAL           TRAN
sda  0:0:0:0    disk SanDisk  SanDisk Ultra PMAP 0000000000000000 usb
{{< /terminal >}}

Next, unmount all partitions on the target drive. Errors about partitions not mounted can be safely ignored.

{{< terminal >}}
$ <kbd>sudo umount /dev/<strong>&lt;device&gt;</strong>?</kbd>
{{< /terminal >}}

Finally, write the disk image to the target drive with `dd`:

{{< terminal >}}
$ <kbd>sudo dd if=<strong>&lt;disk_image.iso&gt;</strong> of=/dev/<strong>&lt;device&gt;</strong> bs=1M oflag=direct conv=fsync status=progress</kbd>
2736783360 bytes (2.7 GB, 2.5 GiB) copied, 54 s, 50.7 MB/s
2634+1 records in
2634+1 records out
2762833920 bytes (2.8 GB, 2.6 GiB) copied, 54.5202 s, 50.7 MB/s
{{< /terminal >}}

**Using the arguments shown here,** the drive will be ready for use and can be safely disconnected as soon as `dd` completes.

<details>
<summary>{{< md >}}Explanation of arguments{{< /md >}}</summary>

<dl>

<dt>{{< md >}}`if`{{< /md >}}</dt>
<dd>{{< md >}}Input file (disk image){{< /md >}}</dd>

<dt>{{< md >}}`of`{{< /md >}}</dt>
<dd>{{< md >}}Output file (target block device){{< /md >}}</dd>

<dt>{{< md >}}`bs=1M`{{< /md >}}</dt>
<dd>{{< md >}}Use a block size of 1 MiB (default 512 B){{< /md >}}</dd>
<dd>{{< md >}}_Tells `dd` to read and write 1 MiB of data at a time. 1 MiB is a good minimum for most modern storage devices, but larger block sizes can further improve transfer speeds on higher capacity drives (which usually have more cache)._{{< /md >}}</dd>

<dt>{{< md >}}`oflag=direct`{{< /md >}}</dt>
<dd>{{< md >}}Use direct I/O for writing (bypassing the kernel cache){{< /md >}}</dd>
<dd>{{< md >}}_The kernel cache makes file operations seem faster than they are in reality, but this is counterproductive when proper operation relies on the data being fully written to disk. Without this flag, the kernel will continue writing data to the drive long after `dd` has exited, and other devices may feel sluggish from kernel cache saturation._{{< /md >}}</dd>

<dt>{{< md >}}`conv=fsync`{{< /md >}}</dt>
<dd>{{< md >}}Flush all data from the drive cache into non-volatile storage before completion{{< /md >}}</dd>

<dt>{{< md >}}`status=progress`{{< /md >}}</dt>
<dd>{{< md >}}Display a live progress indicator{{< /md >}}</dd>

</dl>

---
_See also: [dd man page](https://www.man7.org/linux/man-pages/man1/dd.1.html)_

</details>

</details>

<details>
<summary>{{< md >}}**GUI method** (Linux){{< /md >}}</summary>

### GUI method (Linux) {#flash-gui .hidden-heading}

<small>_This method uses [GNOME Disks](https://apps.gnome.org/app/org.gnome.DiskUtility/) (available in [most distribution repositories](https://pkgs.org/download/gnome-disk-utility "Packaging status for gnome-disk-utility")). You can also use [Mintstick](https://linuxmint-installation-guide.readthedocs.io/en/latest/burn.html#in-linux-mint "Linux Mint &mdash; Mintstick usage instructions") (Linux Mint) or [Etcher](https://www.balena.io/etcher/)._</small>

1. Connect the drive and select it in the left pane of GNOME Disks.
1. From the top-right overflow menu, select <kbd><samp>Restore Disk Image...</samp></kbd>.
1. Select your disk image file.
1. Click <kbd><samp>Start Restoring...</samp></kbd>.

![GNOME Disks screenshot](gnome-disk-utility.webp)
{.text-center}

5. You will see a warning popup --- **confirm once again** that you selected the correct drive and continue.
1. Enter your password to authenticate, then the flashing process will begin.

</details>

<details>
<summary>{{< md >}}Other platforms{{< /md >}}</summary>

### Other platforms {#flash-others .hidden-heading}

{{< padded-list >}}
- **[Etcher](https://www.balena.io/etcher/)** is a cross-platform (Linux/Windows/Mac) Electron-based flashing tool focused on extreme simplicity.
- **[Rufus](https://rufus.ie/)** is a flashing utility for Windows that is both extremely powerful and relatively easy to use.
  - <small>Note: Rufus supports flashing some disk images in 'ISOHybrid' mode. While often faster than DD mode, ISOHybrid flashing causes problems with some disk images. I recommend simply using DD mode unless you intend to edit files on the target drive.</small>
{{< /padded-list >}}

</details>

## Afterword

{{< padded-list >}}
- If you are using a known-good flash drive, there is little need to verify the integrity of the flashed image. However, if you wish to, you can use this Linux command:
  {{< terminal >}}
$ <kbd>sudo head -c `du -b <strong>&lt;disk_image.iso&gt;</strong> | awk '{print $1}'` /dev/<strong>&lt;device&gt;</strong> | sha256sum</kbd>
  22bbee097ed82711df692654fc56332df2133de369de139792005c0ec52cbe68  -
  {{< /terminal >}}
  Etcher will perform this step automatically.
- Many people swear by [Ventoy](https://www.ventoy.net/). **Unless you are frequently working with bootable disk images, do not even consider Ventoy**, because it will offer absolutely no benefit while simultaneously introducing potential problems. Ventoy does not work with all disk images and in some rare cases can even cause issues past the initial boot.
{{< /padded-list >}}
