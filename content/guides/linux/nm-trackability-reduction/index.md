---
title: "NetworkManager Trackability Reduction"
description: "Reduce trackability on and across networks."
lead: ""
draft: false
tags: ["Fedora Silverblue", "Pop!_OS"]
lastmod: 2022-09-04

date: 2022-09-04
menu:
  guides:
    parent: "linux"
weight: 2
toc: true

uuid: "caf7528c-6430-42cc-a43f-03df0ebdc449"
---

<details>
<summary>{{< md >}}**MAC address randomization**<br>
<small>How to mask your hardware MAC address to limit tracking on and across networks.</small>
{{< /md >}}</summary>

## MAC address randomization {#macrand .hidden-heading}

{{< alert context="info" icon="👉" >}}
Note that Ethernet connections can still be tracked via switch ports, and WiFi connections can be broadly localized by access point.

Furthermore, MAC address spoofing and randomization depends on firmware support from the interface. Most modern network interface cards support the feature.
{{< /alert >}}

### Available configurations {#macrand-configurations .hidden-heading}

There are three different aspects of MAC address randomization in NetworkManager, each with their own configuration flag:

#### WiFi scanning {#macrand-scanning .h5}

{{< codetext >}}
[device]
wifi.scan-rand-mac-address=yes
{{< /codetext >}}

#### WiFi connections {#macrand-wifi .h5}

{{< codetext >}}
[connection]
wifi.cloned-mac-address=<strong>&lt;mode&gt;</strong>
{{< /codetext >}}

#### Ethernet connections {#macrand-ethernet .h5}

{{< codetext >}}
[connection]
ethernet.cloned-mac-address=<strong>&lt;mode&gt;</strong>
{{< /codetext >}}

#### Mode options {#macrand-modes}

`random`
: Generate a new random MAC address every time a connection is activated

`stable`
: Assign each connection a random MAC address that will be maintained across activations

`preserve`
: Use the MAC address already assigned to the interface (such as from `macchanger`), or the permanent address if none is assigned

`permanent`
: Use the MAC address permanently baked into the hardware

### Setting a default configuration {#macrand-default-configuration}

It's best to create a dedicated configuration file, such as {{< nowrap >}}`/etc/NetworkManager/conf.d/99-random-mac.conf`{{< /nowrap >}}, to ensure package updates do not overwrite the configuration. In general, I recommend the following:

{{< codetext >}}
[device]
wifi.scan-rand-mac-address=yes

[connection]
wifi.cloned-mac-address=random
ethernet.cloned-mac-address=random
{{< /codetext >}}

This configuration randomizes all MAC addresses by default. These settings can of course be [overridden on a per-connection basis](#macrand-overrides "MAC address randomization&nbsp;&mdash; Per-connection overrides").

After editing the file, run <kbd class="text-nowrap">sudo nmcli general reload conf</kbd> to apply the new configuration.

### Per-connection overrides {#macrand-overrides}

Connection-specific settings take precedence over configuration file defaults. They can be set through {{< nowrap >}}`nm-connection-editor`{{< /nowrap >}} {{< nowrap >}}("Network Connections"){{< /nowrap >}}, a DE-specific network settings GUI, `nmtui`, or `nmcli`.

Look for "Cloned MAC address" under the "Wi-Fi" or "Ethernet" section:

![nm-connection-editor screenshot](nm-connection-editor.webp)
{.text-center}

In addition to the four mode keywords, you can input an exact MAC address to be used for that connection.

For a home or other trusted network, it can be helpful to use `stable` or even `permanent`, as MAC address stability can help avoid being repeatedly served a new IP address and DHCP lease (though not all DHCP servers work this way).

For public networks with captive portals (webpages that must be accessed to gain network access), the `stable` setting can help prevent redirection back to the captive portal after a brief disconnection or roaming to a different access point.

### Seeing the randomized MAC address {#macrand-checking}

Activate the connection in question, and then look for {{< nowrap >}}`GENERAL.HWADDR`{{< /nowrap >}} in the output of <kbd class="text-nowrap">nmcli device show</kbd>. This represents the MAC address currently in use by the interface, whether randomized or not. It is also visible as "Hardware Address" (or similar) in NetworkManager GUIs under active connection details.

{{< terminal >}}
$ <kbd>nmcli device show</kbd>
GENERAL.DEVICE:                         enp5s0
GENERAL.TYPE:                           ethernet
<strong>GENERAL.HWADDR:                         XX:XX:XX:XX:XX:XX</strong>
&hellip;

GENERAL.DEVICE:                         wlp3s0
GENERAL.TYPE:                           wifi
<strong>GENERAL.HWADDR:                         XX:XX:XX:XX:XX:XX</strong>
&hellip;
{{< /terminal >}}

### Sources {#macrand-sources}

- [MAC Address Spoofing in NetworkManager 1.4.0](https://blogs.gnome.org/thaller/2016/08/26/mac-address-spoofing-in-networkmanager-1-4-0/)
- [NetworkManager.conf man page](https://networkmanager.dev/docs/api/latest/NetworkManager.conf.html)
- [ArchWiki&nbsp;--- NetworkManager](https://wiki.archlinux.org/title/NetworkManager#Configuring_MAC_address_randomization)

</details>




<!-- SECTION SEPARATOR -->




<details>
<summary>{{< md >}}**Remove static hostname to prevent hostname broadcast**<br>
<small>How to avoid broadcasting a persistent and likely unique software identifier.</small>
{{< /md >}}</summary>

## Remove static hostname to prevent hostname broadcast {#rmhostname .hidden-heading}

{{< terminal >}}
$ <kbd>sudo hostnamectl hostname "localhost"</kbd>
{{< /terminal >}}

An empty (blank) hostname is also an option, but a static hostname of "localhost" is less likely to cause breakage. Both will result in no hostname being broadcasted to the DHCP server.

### Disabling transient hostname management {#rmhostname-transient}

It's best to create a dedicated configuration file, such as {{< nowrap >}}`/etc/NetworkManager/conf.d/01-transient-hostname.conf`{{< /nowrap >}}, to ensure package updates do not overwrite the configuration:

{{< codetext >}}
[main]
hostname-mode=none
{{< /codetext >}}

This will prevent NetworkManager from setting transient hostnames that may be provided by some DHCP servers. This will have no visible effect except with an empty static hostname.

After editing the file, run <kbd class="text-nowrap">sudo nmcli general reload conf</kbd> to apply the new configuration. Run <kbd class="text-nowrap">sudo hostnamectl \--transient hostname \"\"</kbd> to reset the transient hostname.

### Sources {#rmhostname-sources}

- [NetworkManager.conf man page](https://networkmanager.dev/docs/api/latest/NetworkManager.conf.html)
- [hostnamectl man page](https://www.freedesktop.org/software/systemd/man/hostnamectl)

</details>




<!-- SECTION SEPARATOR -->




<details>
<summary>{{< md >}}
**Disable sending hostname to DHCP server**<br>
<small>How to limit hostname exposure. _Imperfect and hacky&nbsp;--- prefer removing static hostname instead._</small>
{{< /md >}}</summary>

## Disable sending hostname to DHCP server {#sendhostname .hidden-heading}

{{< alert context="danger" icon="👉" >}}
**This configuration will leak your hostname on first connection.** Setting a generic or random hostname is strongly recommended if possible.

Due to [limitations in NetworkManager](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues/584 "NetworkManager issue: No way to set dhcp-send-hostname globally"), it is not possible to reliably disable sending hostnames by default. This setup is very much a hack.
{{< /alert >}}

<small>Due to being leaky, this configuration is virtually useless without also [randomizing MAC addresses by default](#macrand-default-configuration "MAC address randomization&nbsp;&mdash; Setting a default configuration"). Your MAC address and hostname will not be correlated starting with the second connection, assuming the first connection used a random MAC address.</small>

Create {{< nowrap >}}`/etc/NetworkManager/dispatcher.d/no-wait.d/01-no-send-hostname.sh`{{< /nowrap >}} as follows:

{{< codetext >}}
#!/bin/sh

if [ "$(nmcli -g 802-11-wireless.cloned-mac-address c show "$CONNECTION_UUID")" = 'permanent' ] \
        || [ "$(nmcli -g 802-3-ethernet.cloned-mac-address c show "$CONNECTION_UUID")" = 'permanent' ]
then
    nmcli connection modify "$CONNECTION_UUID" \
            ipv4.dhcp-send-hostname true \
            ipv6.dhcp-send-hostname true
else
    nmcli connection modify "$CONNECTION_UUID" \
            ipv4.dhcp-send-hostname false \
            ipv6.dhcp-send-hostname false
fi
{{< /codetext >}}

The script must have specific file permissions and a symlink to take effect:

{{< terminal >}}
$ <kbd>cd /etc/NetworkManager/dispatcher.d/</kbd>

$ <kbd>sudo chown root:root no-wait.d/01-no-send-hostname.sh</kbd>

$ <kbd>sudo chmod 744 no-wait.d/01-no-send-hostname.sh</kbd>

$ <kbd>sudo ln -s no-wait.d/01-no-send-hostname.sh ./</kbd>
{{< /terminal >}}

This script will be automatically triggered on connection events to modify the connection's {{< nowrap >}}`dhcp-send-hostname`{{< /nowrap >}} settings. If the connection's _cloned MAC address_ is [explicitly overridden](#macrand-overrides "MAC address randomization&nbsp;&mdash; Per-connection overrides") to `permanent`, the hostname will be sent to the DHCP server on future connections. In all other cases, the hostname will be masked on future connections, so the DHCP server will only see the MAC address.

### Verifying proper operation {#sendhostname-verification}

After initiating first connection with a network:

{{< terminal >}}
$ <kbd>nmcli c show <strong>&lt;connection&gt;</strong> | grep dhcp-send-hostname</kbd>
ipv4.dhcp-send-hostname:                no
ipv6.dhcp-send-hostname:                no
{{< /terminal >}}

`<connection>` can be the connection name (usually the SSID for WiFi networks) or UUID, obtained from {{< nowrap >}}`nmcli c show [--active]`{{< /nowrap >}}.

_Recall that these setting values are set based on the previous connection activation and take effect for the next connection activation._

### Sources {#sendhostname-sources}

- [NetworkManager: Disable Sending Hostname to DHCP Server](https://viliampucik.blogspot.com/2016/09/networkmanager-disable-sending-hostname.html)
- [NetworkManager-dispatcher man page](https://networkmanager.dev/docs/api/latest/NetworkManager-dispatcher.html)
- [nmcli man page](https://networkmanager.dev/docs/api/latest/nmcli.html)

</details>
