---
title: "Installing Fedora Silverblue 36 on a Surface Pro 3"
description: "Turning the flagship Windows device into a Linux machine."
lead: "Turning the flagship Windows device into a Linux machine."
draft: false
tags: ["Fedora Silverblue"]
lastmod: 2022-09-25

menu:
  guides:
    parent: "linux"
weight: -2
toc: true

uuid: "2699d61f-e7bd-482d-9c36-d713b2cf7c83"
---

## Configuring the UEFI for USB boot

With the device powered off, press the power button while holding volume up to access the UEFI setup. In the UEFI setup, make sure "Configure Alternate System Boot Order" is set to include `USB`, such as the `USB -> SSD` option. If acceptable for your usecase, I recommend enabling "Battery Limit" under "Kiosk Mode" to set a battery charge limit of 50% (to maximize battery longevity). Then save and exit setup.

Alternatively, press the power button while holding volume down to initiate a single boot from USB. ([Boot Surface from a USB device](https://support.microsoft.com/en-us/surface/boot-surface-from-a-usb-device-fe7a7323-8d1d-823d-be17-9aec89c4f9f5))

## Before erasing Windows

### Updating firmware {.h4}

There are 2 ways to update Surface firmware: Windows Update or a set of MSI files provided by Microsoft. [In theory it is possible to use the MSI files with `fwupd`](https://github.com/linux-surface/surface-uefi-firmware "UEFI firmware updates for linux-surface"), but you might as well perform the updates the easy way if Windows is still installed (especially since the Surface Pro 3 is EOL and unlikely to receive future updates). One way or another, I recommend manually updating the firmware instead of trusting Windows Update.

Following the links from the [Microsoft support page](https://support.microsoft.com/en-us/surface/download-drivers-and-firmware-for-surface-09bb2e09-2a4b-cb69-0951-078a7739e120 "Download drivers and firmware for Surface"), I downloaded:
- {{< nowrap >}}`SurfacePro3_Win10_18362_1902002_0.msi`{{< /nowrap >}}
  - This is the latest version available at the time of writing, which works fine for the Windows 10 build 19042 I have installed.
- {{< nowrap >}}`Microsoft_Surface_Pro_3_Tpm_Update_Tool_Setup.msi`{{< /nowrap >}}
  - This is a separate tool for updating the TPM firmware.

Running the main firmware updater is as simple as clicking through the prompts and restarting at the end.

The TPM update, on the other hand, requires its own [detailed guide](https://support.microsoft.com/en-us/topic/install-and-use-the-surface-pro-3-trusted-platform-module-tpm-update-tool-d5e52c61-c7ec-0544-b6e9-e0e0b85cbc10 "Install and use the Surface Pro 3 Trusted Platform Module (TPM) update tool") and a spare flash drive. Note that after running the downloaded installer MSI, the actual utility must be launched from {{< nowrap >}}`C:\Program Files\Microsoft Surface Pro 3 TPM Update Tool\Microsoft.Surface.SP3TpmKey.exe`{{< /nowrap >}} (there is no shortcut).

### Secure&#8209;erasing the internal drive (optional) {.h4 #secure-erasing}

BitLocker is enabled by default on the Surface Pro 3, so unless it was disabled at some point or data was otherwise stored on the drive unencrypted, secure&#8209;erasing is unnecessary. However, there is no harm in performing a secure erase anyway.

Secure&#8209;erasing must be done with the [Microsoft Surface Data Eraser](https://docs.microsoft.com/en-us/surface/microsoft-surface-data-eraser) tool. It is not possible to use standard tools (such as `hdparm` on Linux) due to an ATA security freeze that cannot be lifted by readily apparent means.

## Installing Fedora Silverblue

Since this is a Surface device, the [linux&#8209;surface wiki](https://github.com/linux-surface/linux-surface/wiki) is our friend. Fortunately, the Surface Pro 3 is [fully supported upstream](https://github.com/linux-surface/linux-surface/wiki/Supported-Devices-and-Features#feature-matrix "linux-surface Feature Matrix"), so we should expect full functionality without having to install the {{< nowrap >}}`linux-surface`{{< /nowrap >}} kernel.

[Download the Fedora Silverblue x86_64 ISO and checksum file.](https://silverblue.fedoraproject.org/download) Verify the download and flash it to a spare drive following my [universal disk image procedure]({{< uuid-ref "abe5db40-50ed-46f2-83c6-f0c9afe2913a" >}}).

{{< alert context="info" icon="👉" >}}
You can use the cross-platform [Fedora Media Writer](https://docs.fedoraproject.org/en-US/fedora/latest/install-guide/install/Preparing_for_Installation/#sect-preparing-boot-media) in lieu of downloading and flashing manually.
{{< /alert >}}

Boot into the Fedora Silverblue installer and proceed through the installer as usual. In the "Installation Destination" settings, make sure to select "I would like to make additional space available." and "Encrypt my data." After clicking <kbd><samp>Done</samp></kbd>, set a disk encryption password, then, on the "Reclaim Disk Space" screen, select <kbd><samp>Delete all</samp></kbd> to use the entire drive for Fedora Silverblue. Complete the installation and reboot into the OS, then complete the setup as normal.

{{< alert context="danger" icon="👉" >}}
**I strongly recommend enabling full disk encryption.** Due to the Surface's [notoriously poor repairability](https://www.ifixit.com/Device/Microsoft_Surface_Pro_3 "iFixit: Microsoft Surface Pro 3 Repair"), it is extremely difficult to retrieve or destroy the SSD in the event of hardware failure.
{{< /alert >}}

## Post-installation notes

{{< padded-list >}}
- The touchpad on the Type Cover only has a single physical button. To be able to right&#8209;click effectively, run: <kbd class="text-nowrap">gsettings set org.gnome.desktop.peripherals.touchpad click-method \"areas\"</kbd>
  - By default, using 2 fingers will trigger right&#8209;click and 3 middle&#8209;click, but this is an extremely poor experience due in part to the small size of the touchpad.
- The stylus (Surface Pen) is fully natively supported by libinput, including proximity detection, pressure sensitivity, and the eraser and right&#8209;click buttons. Note that the eraser button requires app support, for example implemented in [Xournal++](https://flathub.org/apps/details/com.github.xournalpp.xournalpp).
  - On Bluetooth-supported versions of the Surface Pen, [additional functionality](https://old.reddit.com/r/Surface/comments/9fhx5p/surface_pen_works_without_bluetooth_on/ "Surface pen works without bluetooth on") (such as the top button) may be unlocked by Bluetooth pairing, but I do not have one to test.
- At the time of writing, the GNOME Software store ("Software") is rather buggy and has a habit of loading indefinitely after certain actions. When that happens, terminate it via System Monitor or <kbd class="text-nowrap">pkill gnome-software</kbd>, after which it should function normally.
- Flathub is disabled and [filtered](https://fedoraproject.org/wiki/Changes/Filtered_Flathub_Applications "Fedora Project Wiki&nbsp;&mdash; Changes/Filtered Flathub Applications") by default. The simplest way to use unfiltered Flathub is by adding it as a new (distinct) remote: <kbd class="text-nowrap">flatpak remote-add flathub-unfiltered https://flathub.org/repo/flathub.flatpakrepo</kbd>
- Use [Flatpak overrides](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-override) (optionally via [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) from unfiltered Flathub) to fine-tune Flatpak app permissions.
  - Since Fedora Silverblue uses Wayland, I recommend globally blocking X11 (XWayland) access and its related IPC permission, only enabling them on a case-by-case basis for apps which are not Wayland-compatible: <kbd class="text-nowrap">flatpak override \--user \--nosocket=x11 \--nosocket=fallback-x11 \--unshare=ipc</kbd>
{{< /padded-list >}}
