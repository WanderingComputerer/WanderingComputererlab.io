---
title: "Installing Pop!_OS 21.10 on an Acer Chromebook 15 (SAND)"
description: "Breaking out of Google's walled garden."
lead: "Breaking out of Google's walled garden."
draft: false
tags: ["Pop!_OS"]
lastmod: 2022-05-18

menu:
  guides:
    parent: "linux"
weight: -1
toc: true

uuid: "9df1c5f0-d061-4147-8e19-d6175f817e88"
---

## Preparation

### Identifying your Chromebook {.h4}

Unlike typical "PC" laptops, every Chromebook is quite different in terms of firmware and the process of installing a traditional Linux distro (without [Crostini](https://chromium.googlesource.com/chromiumos/docs/+/HEAD/containers_and_vms.md "Running Custom Containers Under ChromeOS")), so it is very important to identify _exactly_ what model you have! Luckily this is as easy as navigating to `chrome://system` and looking at the `hardware_class` field --- in my case `SAND`. This can be done in guest mode or as a signed-in user.

{{< alert context="info" icon="👉" >}}
If you have any Chromebook other than SAND, some or all of this guide may not apply to you. Other Apollo Lake models will probably have similar processes, but always check the [GalliumOS wiki](https://wiki.galliumos.org/Installing/Preparing "GalliumOS &mdash; Installing/Preparing") and follow the instructions pertaining to your specific model.
{{< /alert >}}

### Creating Pop!_OS boot media {.h4}

[Download the latest Pop!_OS ISO](https://pop.system76.com/) (normal variant). Verify the download and flash it to a spare drive using my [universal disk image procedure]({{< uuid-ref "abe5db40-50ed-46f2-83c6-f0c9afe2913a" >}}).

## Enabling Developer Mode

{{< alert context="warning" icon="👉" >}}
Before proceeding, backup any important data! **All data saved inside Chrome OS will be permanently erased through this process.**
{{< /alert >}}

With the Chromebook powered off, hold <kbd><kbd>Esc</kbd>+<kbd>Refresh</kbd></kbd> and press the power button to enter recovery mode. Then press <kbd><kbd>Ctrl</kbd>+<kbd>D</kbd></kbd> followed by <kbd>Enter</kbd> to disable OS verification and reboot. There is a 30-second wait and 2 loud beeps on the "OS verification is OFF" screen, followed by even more waiting until the Chromebook finally reboots with Developer Mode enabled. Connect to a WiFi network and proceed through setup until "Browse as Guest" becomes available.

## Disabling write protection

First we must disconnect the battery to allow disabling firmware write protection. With the Chromebook powered on and charging, hold <kbd><kbd>Refresh</kbd>+<kbd>Power</kbd></kbd> while _simultaneously_ unplugging the power supply (this requires 2 hands). After a few seconds, the battery should be cut off; confirm this by trying to boot the Chromebook with the power supply disconnected.

Remove the 11 screws on the bottom with a 2.5&nbsp;mm Phillips driver and **gently** pry off the bottom cover with a guitar pick, credit card, or other non-marring pry tool. **Carefully** ease the battery connector out of its socket using the molded plastic tabs. Remove the battery from the machine (since it is not secured in any way) and flip the machine back over **onto a non-conductive surface**. If you do not have a non-conductive surface available, snap the bottom cover back into place.

![Screws on the bottom of the device](screws.webp)
{.text-center}

![Inside the device](inside.webp)
{.text-center}

Reconnect the power supply and boot into the Guest user. Launch the _crosh_ terminal with <kbd><kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd></kbd>, and run the following commands to disable write protection:

{{< terminal >}}
crosh> <kbd>shell</kbd>
chronos@localhost / $ <kbd>sudo flashrom --wp-disable</kbd>
&hellip;
SUCCESS
{{< /terminal >}}

This will persist even after reconnecting the battery. If you ever wish to re-enable write protection, simply run `sudo flashrom --wp-enable` from the _crosh_ shell (no need to disconnect the battery). You can also run `sudo flashrom --wp-status` to check if write protection is enabled.

Power off the device, disconnect the power supply, and reassemble everything including reconnecting the battery.

## Flashing firmware

Connect the power supply to disable the battery cutoff, and boot the machine. Login as Guest and launch _crosh_ again (<kbd><kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd></kbd>). Run the following commands:

{{< terminal >}}
crosh> <kbd>shell</kbd>
chronos@localhost / $ <kbd>cd; curl -LO mrchromebox.tech/firmware-util.sh && sudo bash firmware-util.sh</kbd>
{{< /terminal >}}

Follow the steps to install the **UEFI (full ROM)** firmware. This will require a flash drive (with a writable FAT32/exFAT/NTFS filesystem) to save a backup of the stock firmware.

After the process finishes, return to the main menu, connect the Pop!_OS bootable drive, and select <kbd>R</kbd> to reboot. As stated in the script, the first boot will display a black screen for quite some time.

## Installing Pop!_OS

If you see an error that booting from eMMC failed, simply press any key to continue. This may drop you to the UEFI shell --- after it finishes scrolling simply type <kbd>exit</kbd> and press <kbd>Enter</kbd> to access the UEFI settings. Changing the boot order is currently bugged and does not save, so just go to <samp>Boot Menu</samp> and select your USB drive to manually boot Pop!_OS.

Proceed through the setup to install Pop!_OS to the internal eMMC drive, `/dev/mmcblk1`. Custom partitioning can be used if desired.

{{< alert context="danger" icon="👉" bold=true >}}
I strongly recommend enabling full disk encryption since there is no standard way to secure-erase eMMC.
{{< /alert >}}

## Post-installation notes

{{< padded-list >}}
- At the time of writing, none of SAND's audio hardware works under Linux. In order to get audio output or input you will need to use Bluetooth or a USB audio adapter (such as those bundled with smartphones).
  - <small>[In theory](https://github.com/GalliumOS/galliumos-distro/issues/364#issuecomment-1059636525 "MrChromebox's firmware validation results") you can make the internal speakers work with [some effort](https://github.com/EMLommers/Apollolake_Audio "Apollo Lake audio fix"). I haven't tried.</small>
- The touchpad only has a single physical button. To get a more traditional right-click experience, run <kbd class="text-nowrap">gsettings set org.gnome.desktop.peripherals.touchpad click-method \"areas\"</kbd>, or configure in GNOME Tweaks under _Keyboard & Mouse_ > _Mouse Click Emulation_.
  - By default, using 2 fingers will trigger right-click and 3 middle-click.
- The media keys are recognized as F1--F10. I am not sure if there is a good way to change this behavior.
{{< /padded-list >}}

## Sources

- [GalliumOS Wiki --- Installing/Preparing](https://wiki.galliumos.org/Installing/Preparing)
- [GalliumOS Wiki --- Hardware Compatibility](https://wiki.galliumos.org/Hardware_Compatibility)
- [GalliumOS --- Apollo Lake Platform Validation](https://github.com/GalliumOS/galliumos-distro/issues/364)
- [MrChromebox's Firmware Utility Script](https://mrchromebox.tech/#fwscript)
- [MrChromebox's firmware --- Supported Devices](https://mrchromebox.tech/#devices)
- [How to disable write protection on modern Chromebooks](https://old.reddit.com/r/PixelBook/comments/7kv944/does_anyone_know_how_to_disable_writeprotection/)
