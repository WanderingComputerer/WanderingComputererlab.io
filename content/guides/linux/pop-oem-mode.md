---
title: "Installing Pop!_OS in OEM Mode"
description: "Configure Pop!_OS to run setup on first boot, like a brand new System76 PC."
lead: "How to configure Pop!_OS to run setup on first boot, like a brand new System76 PC."
draft: false
tags: ["Pop!_OS"]
lastmod: 2022-05-05

date: 2022-05-04
menu:
  guides:
    parent: "linux"
weight: 3
toc: false

uuid: "5cbed2e4-323e-4d23-9b24-a08df465b0d2"
---

{{< alert context="dark" icon="👉" >}}
This procedure applies to UEFI installations only. Legacy boot mode installations [omit the recovery partition](https://github.com/pop-os/pop/issues/936) and cannot easily be configured for the OEM experience.
{{< /alert >}}

{{< padded-list >}}
1. Create and boot Pop!_OS installation media as normal.
1. Perform a clean install, but **do not restart when finished**.
   - The display name, username, and password can be set to anything.
   - Disable encryption, which is pointless at this stage.
1. Mount the recovery partition (usually partition 2). At the root of the recovery partition, locate `recovery.conf`. In it, set `OEM_MODE=1`, then save and exit.
   - GNOME Disks ("Disks") is the easiest way to identify and mount partitions.
1. Mount the EFI system partition (usually partition 1). Relative to the root of the EFI partition:
   1. Navigate to `loader/entries/` and copy the filename without extension of the recovery entry (`Recovery-XXXX-XXXX`).
   1. Edit `loader/loader.conf` by replacing `Pop_OS-current` with the copied text. It should read `default Recovery-XXXX-XXXX`. Save and exit.
1. Shut down the computer and remove the Pop!_OS installation media.
1. (Optional) Perform a test boot to ensure the setup launches.
{{< /padded-list >}}

## Sources

- [How to enable Pop!_OS OEM experience](https://old.reddit.com/r/pop_os/comments/d38av4/is_there_an_oem_install_mode/f00leq1/)
