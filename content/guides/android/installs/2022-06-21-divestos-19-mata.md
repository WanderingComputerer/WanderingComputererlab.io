---
title: "Installing DivestOS 19.1 on an Essential PH‑1 (mata)"
description: "Keeping a perfectly functional phone in service, with strong privacy and security to boot."
lead: "Keeping a perfectly functional phone in service, with strong privacy and security to boot."
draft: false
tags: []
lastmod: 2022-06-21

menu:
  guides:
    parent: "android"
weight: -1
toc: true

uuid: "39198920-0a6a-4ad2-81a3-373fbb65a03d"
---

{{< alert context="warning" icon="👉" >}}
Before proceeding, backup any important data (including pictures and app data)! **All data saved on the device will be permanently erased through this process.**

There is also a nonzero chance of bricking the device, although in most cases it should be recoverable.
{{< /alert >}}

## Preparation

### A word on ADB and fastboot {#adb-fastboot}

{{< padded-list >}}
- The included USB C--C cable **does not work** for fastboot. You will need to try your luck with third-party cables (USB A--C or USB C--C).
- To get ADB and fastboot, use your package manager to install `android-tools` or download the [SDK platform tools from the Android Developers site](https://developer.android.com/studio/releases/platform-tools). Your fastboot version must be 31.0.2 or newer to work with mata (legacy A/B support required).
- Install `android-udev-rules` through your package manager (if available) or [manually from the Git repo](https://github.com/M0Rf30/android-udev-rules "GitHub&nbsp;&mdash; android-udev-rules") to avoid the "no permissions" error and needing to run ADB and fastboot as root.
- Mata has a quirk where fastboot commands sometimes hang. If you experience this, use the volume keys to scroll to "Restart bootloader" and press the power button to select. Then **immediately** rerun the intended fastboot command&nbsp;--- it will succeed as soon as the bootloader finishes restarting. This workaround has been 100% consistent in my testing (when using a compatible cable).
  - For this reason, I recommend [editing the flashall.sh back-to-stock script with a redundant `fastboot flash` command at the beginning](#back-to-stock "Back to stock").
{{< /padded-list >}}

### Downloads

Download the following:

- [Latest DivestOS 19.1 fastboot.zip for mata](https://divestos.org/index.php?page=devices&base=LineageOS#device-mata) and its 512sum file
- [DivestOS GPG public key](https://divestos.org/index.php?page=devices&base=LineageOS#verify)
- [Stock fastboot image collection QQ1A.200105.032](https://essential-images.netlify.app/) [[direct link](https://sourceforge.net/projects/essential-factory-images/files/PH1-Images-QQ1A.200105.032.zip/download "PH1-Images-QQ1A.200105.032.zip")]

Verify each of the downloads&nbsp;--- we want to be extra cautious to minimize the risk of bricking and other issues. Hashes for the stock ROM are posted on [XDA by DivestOS maintainer SkewedZeppelin](https://forum.xda-developers.com/t/rom-divestos-19-1-for-mata.4249419/#post-86808461 "[ROM] DivestOS 19.1 for mata") and the [download site](https://essential-images.netlify.app/ "Essential Factory Images"). Adjust filenames accordingly:

{{< terminal >}}
$ <kbd>gpg --import divestos_signing.key</kbd>
gpg: key 7F627E920F316994: public key "DivestOS Release Signing (2020 #1) &lt;support+releasesigning@divestos.org&gt;" imported
gpg: Total number processed: 1
gpg:               imported: 1

$ <kbd>gpg --verify divested-19.1-20220601-dos-mata-fastboot.zip.sha512sum</kbd>
gpg: Signature made Wed 01 Jun 2022 02:30:58 AM UTC
gpg:                using EDDSA key B8744D67F9F1E14E145DFD8E7F627E920F316994
gpg: Good signature from "DivestOS Release Signing (2020 #1) <support+releasesigning@divestos.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: B874 4D67 F9F1 E14E 145D  FD8E 7F62 7E92 0F31 6994

$ <kbd>sha512sum -c divested*sha512sum</kbd>
divested-19.1-20220601-dos-mata-fastboot.zip: OK
sha512sum: WARNING: 10 lines are improperly formatted

$ <kbd>echo 'a9d979fdde4b2b59ff9c0c1256f440b4d5250242179648494a9b641ca75b4911cae666b6197162b6a93009b94cdc07f9f04df2bd0a72e819db09f7392f60ddde' 'PH1-Images-QQ1A.200105.032.zip' | sha512sum -c</kbd>
PH1-Images-QQ1A.200105.032.zip: OK

$ <kbd>echo '3b02ec475e8ae567f709e7cdfef1316f177a30f4bf797ab44e372735375f2ace' 'PH1-Images-QQ1A.200105.032.zip' | sha256sum -c</kbd>
PH1-Images-QQ1A.200105.032.zip: OK
{{< /terminal >}}

## Unlocking the bootloader

{{< alert context="primary" icon="👉" >}}
Even if your bootloader is already unlocked, you should follow these steps to ensure the 'critical' partitions are unlocked. **This is necessary to guarantee a successful back-to-stock operation.**
{{< /alert >}}

Go to <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>About phone</samp></kbd></kbd>, and tap <kbd><samp>Build number</samp></kbd> 7 times to unlock developer options. In the developer options (under "System"), enable <kbd><samp>OEM unlocking</samp></kbd>. Then restart the device while holding the volume down key to access the bootloader, and connect to the PC with the USB cable.

Run {{< nowrap >}}<kbd>fastboot flashing unlock</kbd>{{< /nowrap >}}. If you get a {{< nowrap >}}"<samp>Device already : unlocked!</samp>"{{< /nowrap >}} error, simply move on to the next command. Otherwise choose <kbd><samp>Yes</samp></kbd> and wait for the phone to finish restarting, then restart into the bootloader once again.

Run {{< nowrap >}}<kbd>fastboot flashing unlock_critical</kbd>{{< /nowrap >}}. If you get a {{< nowrap >}}"<samp>Device already : unlocked!</samp>"{{< /nowrap >}} error, you are done. Otherwise choose <kbd><samp>Yes</samp></kbd> and wait for the phone to finish restarting.

## Back to stock

Extract {{< nowrap >}}`PH1-Images-QQ1A.200105.032.zip`{{< /nowrap >}} and change directory into the extracted folder.

To work around mata's [fastboot quirk](#adb-fastboot "A word on ADB and fastboot"), add a redundant {{< nowrap >}}`fastboot flash`{{< /nowrap >}} command to the beginning of {{< nowrap >}}`flashall.sh`{{< /nowrap >}}:

{{< codetext >}}
#!/bin/bash
<kbd>fastboot flash boot_a boot.img</kbd>
fastboot flash nvdef_a nvdef.img
fastboot flash nvdef_b nvdef.img
fastboot flash boot_a boot.img
&hellip;
{{< /codetext >}}

Save and exit. Run {{< nowrap >}}`flashall.sh`{{< /nowrap >}} and [restart the bootloader if necessary](#adb-fastboot "A word on ADB and fastboot"):

{{< terminal >}}
$ <kbd>cd PH1-Images-QQ1A.200105.032/</kbd>

$ <kbd>./flashall.sh</kbd>
Sending 'boot_a' (23885 KB)                        OKAY [  0.185s]
Writing 'boot_a'                                   OKAY [  0.310s]
Finished. Total time: 0.515s
Sending 'nvdef_a' (310 KB)                         OKAY [  0.088s]
&hellip;
Writing 'userdata'                                 OKAY [  0.057s]
Finished. Total time: 0.293s
Rebooting                                          OKAY [  0.000s]
Finished. Total time: 0.050s
{{< /terminal >}}

The phone will reboot into the OS at this point.

## Installing DivestOS

Restart into the bootloader and push the {{< nowrap >}}fastboot.zip{{< /nowrap >}}, [restarting the bootloader and rerunning the command if necessary](#adb-fastboot "A word on ADB and fastboot"):

{{< terminal >}}
$ <kbd>fastboot update divested-19.1-20220601-dos-mata-fastboot.zip</kbd>
--------------------------------------------
Bootloader Version...: mata-34cc3f5
Baseband Version.....: 2.0.c4-M2.0.10
Serial Number........: XXXXXXXXXXXXXXXX
--------------------------------------------
extracting android-info.txt (0 MB) to RAM...
Checking 'product'                                 OKAY [  0.000s]
&hellip;
archive does not contain 'vendor_other.img'
Rebooting                                          OKAY [  0.000s]
Finished. Total time: 29.959s
{{< /terminal >}}

{{< alert context="dark" icon="👉" >}}
Normally we would flash the main DivestOS zip file via recovery at this stage, but the sideload feature is broken in the DivestOS recovery for mata. So we perform an OTA update via the Updater app instead.
{{< /alert >}}

The phone will reboot twice before showing an error "<samp>Can't load Android system. Your data may be corrupt</samp>..." Use the volume keys to highlight <kbd><samp>Factory data reset</samp></kbd> and press the power key to select. Confirm by selecting <kbd><samp>Format data</samp></kbd> on the next screen. The phone will reboot into DivestOS.

Proceed through the setup wizard. Avoid setting up personal data and settings at this stage because user data will be wiped again, but it is necessary to connect to the internet via WiFi or mobile data. Go to <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>System</samp></kbd> > <kbd><samp>Updater</samp></kbd></kbd> and download the latest DivestOS version available. Install it after the download finishes, then finally reboot.

### Relocking the bootloader

Before relocking, verify that all functionality works properly: touch, brightness, WiFi, Bluetooth, GPS, camera, microphone, rotation, fingerprint, etc.

Restart into the bootloader. Run {{< nowrap >}}<kbd>fastboot flashing lock</kbd>{{< /nowrap >}} and choose <kbd><samp>Yes</samp></kbd>. Upon reboot, press the power key to pause boot when directed, and compare the displayed cryptographic ID against the [published verified boot hash](https://divestos.org/index.php?page=verified_boot_hashes). It may require multiple boots to compare the full hash.

Restart into the bootloader once last time. Run {{< nowrap >}}<kbd>fastboot flashing lock_critical</kbd>{{< /nowrap >}} and choose <kbd><samp>Yes</samp></kbd>.

{{< alert context="dark" icon="👉" >}}
{{< nowrap >}}`fastboot flashing lock`{{< /nowrap >}} alone will block all fastboot flashing attempts, but the critical partitions will be unlockable by merely running {{< nowrap >}}`fastboot flashing unlock`{{< /nowrap >}}.

That is why I recommend also running {{< nowrap >}}`fastboot flashing lock_critical`{{< /nowrap >}}, which will keep the critical partitions locked even with the bootloader unlocked. Critical partitions should only ever be flashed for a back-to-stock operation.
{{< /alert >}}

## Post-installation notes

{{< padded-list >}}
- Activate developer options by tapping <kbd><samp>Build number</samp></kbd> 7 times in <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>About phone</samp></kbd></kbd>. In the developer options:
  - **Do not** disable OEM unlocking. This is an important precaution for recovering from a bricked state (since the recovery is broken).
  - Disable <kbd><samp>USB debugging</samp></kbd> (ADB) unless in active use.
  - _The developer options menu can be turned off if desired. The settings themselves will be preserved._
- Under <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>Security</samp></kbd></kbd>:
  - Enable <kbd><samp>Auto reboot</samp></kbd> to fully encrypt user data and disable fingerprint unlock after a configurable period of inactivity.
  - Enable secure app spawning.
- In <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>About phone</samp></kbd></kbd>, change the device name to a random string. This will be visible to all connected networks.
  - Change the device name in the Bluetooth settings as well. This will be visible to all nearby Bluetooth devices when in pairing mode.
- Under <kbd><kbd><samp>Settings</samp></kbd> > <kbd><samp>Privacy</samp></kbd> > <kbd><samp>Trust</samp></kbd></kbd>, change <kbd><samp>Restrict USB</samp></kbd> to _deny_ or _only allow when unlocked_.
- Choose a specific provider for Private DNS or turn it off entirely.
  - If you wish to use it, I recommend [Quad9](https://www.quad9.net/). Alternatively, [NextDNS](https://nextdns.io/) offers customizable filterlists.
  - _[Nebulo](https://git.frostnerd.com/PublicAndroidApps/smokescreen) offers a much more robust filterlist and encrypted DNS implementation using the VPN slot._
- Enable Seedvault backup.
- Disable <kbd><samp>Extend compatibility</samp></kbd> in the WiFi hotspot settings unless you need 2.4&nbsp;GHz support.
- Use [Neo Store](https://github.com/NeoApplications/Neo-Store) (formerly Droid&#8209;ify) to download and [seamlessly update](https://www.xda-developers.com/android-12-alternative-app-stores-update-apps-background/ "Android 12 will finally let alternative app stores update apps without bothering the user") F&#8209;Droid apps.
  - Add the [DivestOS WebView repository](https://divestos.org/fdroid/webview/?fingerprint=FB426DA1750A53D7724C8A582B4D34174E64A84B38940E5D5A802E1DFF9A40D2) to receive WebView updates.
- Install and run [AirGuard](https://github.com/seemoo-lab/AirGuard) if daily&#8209;driving the device. ([Why?](https://positive.security/blog/find-you "Find You: Building a stealth AirTag clone"))
- Set up [FindMyDevice](https://gitlab.com/Nulide/findmydevice) and/or [Wasted](https://github.com/x13a/Wasted) for remotely locating, locking, and wiping the device.
- Use [Shelter](https://gitea.angry.im/PeterCxy/Shelter/) or separate user profiles to isolate untrusted apps.
- Use the [GrapheneOS app store](https://github.com/GrapheneOS/Apps) to reinstall the [Camera](https://github.com/GrapheneOS/Camera "GrapheneOS Camera app") app (for [seamless updates](https://www.xda-developers.com/android-12-alternative-app-stores-update-apps-background/ "Android 12 will finally let alternative app stores update apps without bothering the user")) and install the [GrapheneOS PDF Viewer](https://github.com/GrapheneOS/PdfViewer).
{{< /padded-list >}}

## See also

- [XDA Forums&nbsp;--- DivestOS 19.1 for mata](https://forum.xda-developers.com/t/rom-divestos-18-1-19-1-for-mata.4249419/)
- [DivestOS installation instructions (generalized)](https://divestos.org/index.php?page=bootloader)
- [Mirrors of Essential OTAs and other files](https://old.reddit.com/r/essential/comments/i1my5m/essential_developer_image_archives_ota_drivers/)
- [(Old) FAQ for invisiblek's Unofficial LineageOS for mata](https://mata.readthedocs.io/)
