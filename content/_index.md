---
title : "FOSS Guides & More"
description: "Tutorials and commentary about my computing adventures"
lead: "Tutorials and commentary about my computing adventures"
meta: "Open-source, contributions welcome: <a href=\"https://gitlab.com/WanderingComputerer/WanderingComputerer.gitlab.io\" target=\"_blank\">GitLab</a>"
---
